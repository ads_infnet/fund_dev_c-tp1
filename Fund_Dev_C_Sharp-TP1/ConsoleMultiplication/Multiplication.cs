﻿using System;

namespace ConsoleMultiplication
{
    class Multiplication
    {
        static void Main(string[] args)
        {
            double result = 0;

            Console.WriteLine("\n Insira um número:");
            int firstNumber = int.Parse(Console.ReadLine());
            result = firstNumber;


            Console.WriteLine("\n Insira outro número para multiplicar:");
            var secNumber = int.Parse(Console.ReadLine());
            result *= secNumber;

            Console.WriteLine("\n [ A multiplicação é igual a {0}! ]", result);

            Console.WriteLine("\n -------------------------- \n Pressione qualquer tecla pra sair.");
            Console.ReadKey();
        }
    }
}
